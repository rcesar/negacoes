/*
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

using Gtk;

public class DlgNegociacao:Dialog {

    public Entry papel = new Entry();
    public SpinButton quantidade = new SpinButton.with_range(0,99999,1);
    public Entry valor = new Entry();
    public Entry data = new Entry();

    public DlgNegociacao(Window parent)
    {
        //base.Dialog(parent);
        set_size_request(400,200);
        set_title("Registro de negociação");
        var fr1 = new Frame("Papel:");
        var fr2 = new Frame("Quantidade negociada:");
        var fr3 = new Frame("Valor por titulo:");
        var fr4 = new Frame("Data:");
        fr1.add(this.papel);
        fr2.add(this.quantidade);
        fr3.add(this.valor);
        fr4.add(this.data);
        var wbase = get_content_area();
        wbase.pack_start(fr1,false,true);
        wbase.pack_start(fr2,false,true);
        wbase.pack_start(fr3,false,true);
        wbase.pack_start(fr4,false,true);
        add_buttons("gtk-ok",1,"gtk-cancel",0);
    }
    public new  int run()
    {
        show_all();
        return(base.run());
    }
}

