/*
 * main.vala
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
 using Gtk;
 using Sqlite;

extern string abs_path(string path);

Database db;

public class MainWindow:Window {
    private TreeView lista = new TreeView();
    string app_path;
    public MainWindow(string app_path)
    {
        this.app_path=app_path;
        set_title("Controle de ações");
        set_size_request(800,600);
        var wbase = new Box(Orientation.VERTICAL,0);
        //Barra de ferramentas
        var tb = new Toolbar();
        var bt = new ToolButton( new Image.from_icon_name("gtk-quit",IconSize.LARGE_TOOLBAR),"");
        bt.clicked.connect(this.destroy);
        var bt1 = new ToolButton( new Image.from_file(app_path+"/Imagens/compra.png"),"Comprar");
        bt1.clicked.connect(registra_compra);
        var bt2 = new ToolButton( new Image.from_file(app_path+"/Imagens/venda.png"),"Vender");
        bt2.clicked.connect(registra_venda);
        var bt3 = new ToolButton( new Image.from_file(app_path+"Imagens/trocar.png"),"Troca carteira");
        bt3.clicked.connect(()=>{conecta_db();});//conecta_db retorna booleano o conecta do botao precisa de retorno void por isso o lambda
        tb.add(bt);
        tb.add(new SeparatorToolItem());
        tb.add(bt1);
        tb.add(bt2);
        tb.add(new SeparatorToolItem());
        tb.add(bt3);
        wbase.pack_start(tb,false,true);
        var scw = new ScrolledWindow(null,null);
        scw.set_policy(PolicyType.AUTOMATIC,PolicyType.AUTOMATIC);
        scw.add(lista);
        wbase.pack_start(scw,true,true);
        add(wbase);
        //Colunas da lista
        lista.insert_column_with_attributes(-1,"Papel",new Gtk.CellRendererText (), "text",0);
        lista.insert_column_with_attributes(-1,"Quantidade", new Gtk.CellRendererText(),"text",1);
        lista.insert_column_with_attributes(-1,"Preço médio",new Gtk.CellRendererText(),"text",2);
    }

    public bool conecta_db()
    {
      var dlg = new DLG_Select_DB(app_path);
      if (dlg.run()==1)
      {
        string path = dlg.get_db_path();
        if (path==null){dlg.destroy();return(false);}
        //stdout.printf(path);
        int ret = Database.open(path,out db);
        if (ret!=Sqlite.OK)
        {
            stderr.printf((string)db.errmsg()+"\n");
            dlg.destroy();
            return(false);
        }
        atualizar_listagem();
        dlg.destroy();
        return(true);
      } else
      {
        dlg.destroy();
        return(false);
      }
    }

    private void registra_compra()
    {
        Statement stm;
        int quantidade=0;
        float preco_medio=0;
        var dlg = new DlgNegociacao(this);
        string query = "";
        dlg.set_title("Registro de compra de ativos");
        if (dlg.run()==1)
        {
            db.exec("SELECT quantidade,preco FROM estoque WHERE papel='"+dlg.papel.get_text()+"'",(tam,valores,nomes)=>{
                quantidade=int.parse(valores[0]);
                preco_medio=int.parse(valores[1]);
                return(Sqlite.OK);
            });
            //stdout.printf("%i",quantidade);
            if (quantidade==0)
            {
                quantidade=int.parse(dlg.quantidade.get_text());
                preco_medio=float.parse(dlg.valor.get_text());
                query = "INSERT INTO estoque(papel,quantidade,preco) VALUES($papel,$quantidade,$preco)";
                db.prepare_v2(query,query.length,out stm);
                stm.bind_text(1,dlg.papel.get_text());
                stm.bind_text(2,"%i".printf(quantidade));
                stm.bind_text(3,"%.2f".printf(preco_medio));
                if (stm.step()!=Sqlite.DONE)
                {
                    var dl = new MessageDialog(this,DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
                    dl.run();dl.destroy();
                }
            } else {
                int qcomprada = int.parse(dlg.quantidade.get_text());
                float pcomprado = float.parse(dlg.valor.get_text());
                preco_medio = ((quantidade*preco_medio)+(qcomprada*pcomprado))/(quantidade+qcomprada);
                quantidade+=qcomprada;
                query = "UPDATE estoque set quantidade=$QUANTIDADE,preco=$PRECO WHERE papel=$PAPEL";
                db.prepare_v2(query,query.length,out stm);
                stm.bind_text(1,"%i".printf(quantidade));
                stm.bind_text(2,"%.2f".printf(preco_medio));
                stm.bind_text(3,dlg.papel.get_text());
                if (stm.step()!=Sqlite.DONE)
                {
                    var dl = new MessageDialog(this,DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
                    dl.run();dl.destroy();
                }
            }
            query = "INSERT INTO compra(papel,quantidade,valor,data) VALUES($PAPEL,$QUANTIDADE,$VALOR,$DATA)";
            db.prepare_v2(query,query.length,out stm);
            stm.bind_text(1,dlg.papel.get_text());
            stm.bind_text(2,"%i".printf(quantidade));
            stm.bind_text(3,dlg.valor.get_text());
            stm.bind_text(5,dlg.data.get_text());
           if (stm.step()!=Sqlite.DONE)
            {
                var dl = new MessageDialog(this,DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,db.errmsg());
                dl.run();dl.destroy();
            }
        }
        dlg.destroy();
        atualizar_listagem();
    }

    private void registra_venda()
    {
        var md=this.lista.get_model();
        var it = TreeIter();
        var resultado = this.lista.get_selection().get_selected(out md,out it);
        if (resultado)
        {
            var dlg = new DlgNegociacao(this);
            dlg.set_title("Registro de venda de ativos");
            GLib.Value papel;
            md.get_value(it,0,out papel);
            dlg.papel.set_text((string)papel);
            dlg.papel.set_editable(false);
            int quantidade=0;
            string preco="";
            if (dlg.run()==1)
            {
                db.exec("SELECT quantidade,preco FROM estoque WHERE papel='"+dlg.papel.get_text()+"'",(tam,valores,nomes)=>{
                    quantidade=int.parse(valores[0]);
                    preco = valores[1];
                    return(Sqlite.OK);
                });
                if ((quantidade-int.parse(dlg.quantidade.get_text()))<0)
                {
                    var dl = new MessageDialog(this,DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,"Não há o papel em estoque\nVerifique");
                    dl.run();dl.destroy();
                } else {
                    string query1 = "UPDATE estoque SET quantidade=$QUANTIDADE WHERE papel=$PAPEL";
                    string query2 = "INSERT INTO venda VALUES($PAPEL,$QUANTIDADE,$VALOR,$PRECO_MEDIO,$DATA)";
                    Statement stm;
                    db.prepare_v2(query1,query1.length,out stm);
                    stm.bind_text(1,"%i".printf(quantidade-int.parse(dlg.quantidade.get_text())));
                    stm.bind_text(2,dlg.papel.get_text());
                    var ret1 = stm.step();
                    db.prepare_v2(query2,query2.length,out stm);
                    stm.bind_text(1,dlg.papel.get_text());
                    stm.bind_text(2,dlg.quantidade.get_text());
                    stm.bind_text(3,dlg.valor.get_text());
                    stm.bind_text(4,preco);
                    stm.bind_text(5,dlg.data.get_text());
                    var ret2=stm.step();
                    if (ret1!=Sqlite.DONE) {
                        var dl = new MessageDialog(this,DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,"Erro atualizando o estoque de papeis");
                        stderr.printf("%s",db.errmsg());
                        dl.run();dl.destroy();
                    }
                    if (ret2!=Sqlite.DONE) {
                        var dl = new MessageDialog(this,DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,"Erro registrando a venda");
                        stderr.printf("%s",db.errmsg());
                        dl.run();dl.destroy();
                    }
                }
            }
            dlg.destroy();
            atualizar_listagem();
        } else {
                    var dl = new MessageDialog(this,DialogFlags.MODAL,MessageType.ERROR,ButtonsType.OK,"Selecione um papel em estoque para registrar a venda!");
                    dl.run();dl.destroy();
                }
    }

    private void atualizar_listagem()
    {
        TreeIter it = TreeIter();
        var md = new Gtk.ListStore(3,typeof(string),typeof(string),typeof(string));
        db.exec("SELECT papel,quantidade,preco FROM estoque ORDER BY papel",(tam,valores,nomes)=>{
            md.append(out it);
            md.set(it,0,valores[0]);
            md.set(it,1,valores[1]);
            md.set(it,2,valores[2]);
            return(Sqlite.OK);
        });
        lista.set_model(md);
    }
}

public static int main(string[] args)
{
    Gtk.init(ref args);
    string app_path =abs_path(Path.get_dirname(abs_path(args[0])));
    app_path=app_path[0:(app_path.length-3)];
    var jn = new MainWindow(app_path);
    jn.destroy.connect(main_quit);
    jn.show_all();
    Gdk.Pixbuf image = null;
    try {
        image=new Gdk.Pixbuf.from_file(app_path+"Imagens/icone.png");
        jn.set_icon(image);
    }catch(Error e)  { stderr.printf("Erro abrindo arquivo do icone do aplicativo\n");}
    if (jn.conecta_db())
    {
        Gtk.main();
    }
    return(0);
}
