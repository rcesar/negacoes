using Gtk;
using Sqlite;

extern Gtk.ListStore lista_arquivos(string path);

public class DLG_Select_DB:Dialog {
     string app_path ="";
     private TreeView lista = new TreeView();
     public DLG_Select_DB(string path)
     {
         this.app_path=path;
         set_title("Escolha a carteira:");
         set_size_request(800,600);
         var wbase = get_content_area();
         var scw = new ScrolledWindow(null,null);
         Frame fr = new Frame("Carteiras disponiveis:");
         fr.add(scw);
         scw.add(lista);
         lista.insert_column_with_attributes(-1,"",new CellRendererText(),"text",0);
         lista.set_headers_visible(false);
         wbase.pack_start(fr,true,true);
         var bt = new ToolButton( new Image.from_icon_name("gtk-new",IconSize.LARGE_TOOLBAR),"Nova carteira");
         wbase.pack_end(bt,false,false);
         bt.clicked.connect(nova);
         add_buttons("gtk-open",1,"gtk-quit",0);
         Gdk.Pixbuf image = null;
        try {
            image=new Gdk.Pixbuf.from_file(path+"Imagens/icone.png");
            this.set_icon(image);
        }catch(Error e)  { stderr.printf("Erro abrindo arquivo do icone do aplicativo\n");}
     }

     public string get_db_path()
     {
        Gtk.TreeIter it = TreeIter();
        Gtk.TreeModel md;
        string retorno;
        bool resultado=lista.get_selection().get_selected(out md,out it);
        if (resultado)
        {
            GLib.Value dbname;
            md.get_value(it,0,out dbname);
            retorno=this.app_path+"db/"+(string)dbname+".db";
            return(retorno);
        }
      return((string)null);
     }

     public new int run()
     {
      show_all();
      atualiza();
      return(base.run());
     }

     private void nova()
     {
         Dialog dlg = new Dialog();//this.get_toplevel);
         Frame fr = new Frame("Qual o nome da carteira:");
         Entry et = new Entry();
         fr.add(et);
         dlg.get_content_area().pack_start(fr,true,true);
         dlg.set_title("Nova carteira");
         dlg.add_buttons("gtk-ok",1,"gtk-cancel",0);
         dlg.show_all();
         if (dlg.run()==1)
         {
             string pt=app_path+"/db/"+et.get_text()+".db";
             create_db(pt);
             atualiza();
         }
         dlg.destroy();
     }

     private void atualiza()
     {
         Gtk.ListStore md = lista_arquivos(this.app_path);
         lista.set_model(md);
     }

     private void create_db(string path)
     {
        int ret = Database.open(path,out db);
        if (ret!=Sqlite.OK)
        {
            stderr.printf((string)db.errmsg()+"\n");
            return;
        } else
        {
            ret = db.exec("CREATE TABLE IF NOT EXISTS compra(papel CHAR(10),quantidade INT(10),valor FLOAT(10.2),data date)");
            if (ret!=Sqlite.OK) { stderr.printf(db.errmsg());}
            ret = db.exec("CREATE TABLE IF NOT EXISTS venda(papel CHAR(10),quantidade INT(10),valor FLOAT(10.2),preco_medio FLOAT(10.2),data date)");
            if (ret!=Sqlite.OK) { stderr.printf(db.errmsg());}
            ret = db.exec("CREATE TABLE IF NOT EXISTS estoque(papel CHAR(10),quantidade int(20),preco FLOAT(10.2))");
            if (ret!=Sqlite.OK) { stderr.printf(db.errmsg());}
       }
    }
}
