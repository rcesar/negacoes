#include <stdlib.h>
#include <string.h>
#include <glob.h>
#include <gtk/gtk.h>
//#include <stdio.h>

char * abs_path(char *pt)
{
    char *ptr;
    ptr = realpath(pt,NULL);
    if (ptr==NULL) {
        return(NULL);
    } else { return(ptr);}
}

GtkListStore  * lista_arquivos(char *path)
{
    GtkListStore * itens = gtk_list_store_new(1,G_TYPE_STRING);
    GtkTreeIter iter; // nescessario para "gravar" os valores
    int indice=0;
    char *item;
    glob_t arquivos;
    //-----------
    memset(&arquivos, 0, sizeof(arquivos));
    char *cpath=(char*)malloc((strlen(path)+10)*sizeof(char));
    sprintf(cpath,"%s/db/*.db",path);
    int i1 = strlen(cpath)-4;//indice para remover o path e a extenção do arquivo
    int ret = glob(cpath, GLOB_TILDE, NULL, &arquivos);
    free(cpath);
    if(ret != 0)
    {
        globfree(&arquivos);
        puts("Erro listando arquivos");
    }
    for(size_t i = 0; i < arquivos.gl_pathc; ++i) {
        item=(char*)(arquivos.gl_pathv[i]);
        gtk_list_store_append (itens, &iter);
        memmove(item , item + i1, strlen(item)-(strlen(item)-i1)+ 1);//move o fim da string para o começo apagando o path
        item[strlen(item)-3]=0;//Diminui o tamanho cortando o db
        gtk_list_store_set (itens, &iter,0,item,-1);
    }
    //globfree(&arquivos);
    return(itens);
}

